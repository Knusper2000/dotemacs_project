(server-start)

(setq custom-file "~/.gnu-emacs-custom")
(load custom-file)

(defun ensc/mailcap-mime-data-filter (filter)
  ""
  (mapcar (lambda(major)
        (append (list (car major))
            (remove nil
                (mapcar (lambda(minor)
      		(when (funcall filter (car major) (car minor) (cdr minor))
                    minor))
                    (cdr major)))))
mailcap-mime-data))

(defun ensc/no-pdf-doc-view-filter (major minor spec)
  (if (and (string= major "application")
 (string= minor "pdf")
 (member '(viewer . doc-view-mode) spec))
nil
    t))

(eval-after-load 'mailcap
  '(progn
     (setq mailcap-mime-data
 (ensc/mailcap-mime-data-filter 'ensc/no-pdf-doc-view-filter))))

;; need to use mirrorservice in India, for some reason
(add-to-list 'package-archives '("melpa" . "http://www.mirrorservice.org/sites/melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "http://www.mirrorservice.org/sites/stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("nongnu-elpa" . "http://elpa.nongnu.org/nongnu/"))

(use-package gnu-elpa-keyring-update
 :ensure t)

(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))
(require 'use-package)

(use-package paradox
  :ensure t
  :pin melpa-stable
  :init
  (paradox-enable))

(use-package latex
  ;; :ensure auctex
  ;; ignore updates and use a manually installed version (debian package)
  :pin manual
  :init
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (setq LaTeX-math-menu-unicode t)
  (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
  (require 'reftex)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (setq reftex-plug-into-AUCTeX t)
  (setq reftex-cite-format 'natbib)
)

(with-eval-after-load "tex"
  (add-to-list 'TeX-view-program-list '("mupdf" "/usr/bin/mupdf %o"))
  (setcdr (assq 'output-pdf TeX-view-program-selection) '("mupdf"))
  (TeX-add-style-hook
  ;;Astronomy & Astrophyscics 
  "aa"
  (lambda ()
  (TeX-run-style-hooks "natbib" "graphicx" "geometry")))
  )

(defun my-beamer-mode ()
  (require 'reftex)
  (set (make-local-variable 'reftex-section-levels)
     '(("lecture" . 1) ("frametitle" . 2)))
  (reftex-reset-mode)
  )

(eval-after-load "tex"
  '(TeX-add-style-hook "beamer" 'my-beamer-mode))

(use-package org
  :ensure t
  :pin manual
  :config
  ;; we need to override some key bindings
  ;; these are my windmove key-bindings
  (define-key org-mode-map (kbd "<C-S-right>") nil)
  (define-key org-mode-map (kbd "<C-S-left>") nil)
  (define-key org-mode-map (kbd "<C-S-up>") nil)
  (define-key org-mode-map (kbd "<C-S-down>") nil)
  ;; this is how I like to scroll
  (define-key org-mode-map (kbd "<M-down>") nil)
  (define-key org-mode-map (kbd "<M-up>") nil)
  )

(add-hook 'org-agenda-mode-hook
	  '(lambda ()
	     (define-key org-agenda-mode-map (kbd "<C-S-right>") nil)
	     (define-key org-agenda-mode-map (kbd "<C-S-left>") nil)
	     (define-key org-agenda-mode-map (kbd "<C-S-up>") nil)
	     (define-key org-agenda-mode-map (kbd "<C-S-down>") nil)
	     )
)

(use-package org-tempo
  :after org)

(use-package org-cliplink
  :ensure t
  :custom
  (org-cliplink-max-length nil))

(use-package toc-org
  :ensure t
  :pin nongnu-elpa
  :config
  (setq toc-org-hrefify-default "org")
  )

(use-package orgtbl-aggregate
  :ensure t)

(use-package org-inline-pdf
  :ensure t
  :pin melpa-stable
)

(use-package crux
  :ensure t
  :pin melpa-stable
  )

(use-package ivy
  :ensure f
  :init
  (ivy-mode 1)
  :diminish ivy-mode
  )

(use-package counsel
  :ensure f
  :config
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
  )

(use-package swiper
  :ensure f
  :config
  (global-set-key (kbd "C-s") 'swiper)
  )

(use-package smex
  :ensure f
  :pin manual
  )

(use-package company
  :ensure f
  :pin melpa-stable
  )

(use-package company-box
  :ensure t
  :hook (company-mode . company-box-mode))

(use-package anaconda-mode
  :ensure t
  :pin melpa-stable
  :config
  (add-hook 'python-mode-hook 'anaconda-mode)
  ;; (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
  (setq anaconda-mode-lighter "AC")
  )

(use-package company-anaconda
  :ensure t
  :after company
  :config
  (add-to-list 'company-backends 'company-anaconda)
  )

(use-package blacken
  :ensure t
  :pin melpa-stable
  )

(use-package pyvenv
  :ensure t
  :pin melpa-stable
  :init
  (setenv "WORKON_HOME" "~/miniconda3/envs/")  ;; this is where I have my miniconda envs
  (pyvenv-mode 1)
  (setq pyvenv-post-activate-hooks
        (list (lambda ()
                (setq blacken-executable (concat pyvenv-virtual-env "bin/black")))))
  (setq pyvenv-post-deactivate-hooks
	(list (lambda ()
		(setq blacken-executable "black"))))
  )

(use-package restart-emacs
  :ensure t
  :pin melpa-stable
  )

(use-package try
  :ensure t
  :pin melpa-stable
  )

(use-package ebib
  :ensure t
  :pin melpa-stable
  :config
  (setq ebib-index-columns
	(quote
	 (("timestamp" 12 t)
	  ("Entry Key" 20 t)
	  ("Author/Editor" 40 nil)
	  ("Year" 6 t)
	  ("Title" 50 t))))
  (setq ebib-index-default-sort (quote ("timestamp" . descend)))
  (setq ebib-index-window-size 28)
  (setq ebib-preload-bib-files (quote ("~/science_works/bibliography.bib")))
  (setq ebib-timestamp-format "%Y.%m.%d")
  (setq ebib-use-timestamp t)
  (setq ebib-uniquify-keys t)
  )

(use-package yascroll
  :ensure t
  :pin melpa-stable
  :config
  (global-yascroll-bar-mode 1)
  (setq yascroll:delay-to-hide 0.8)
  )

(use-package magit 
  :ensure f
  :pin manual
  :bind ("C-x g" . magit-status)
  )

(use-package buffer-move
  :ensure t
  :pin melpa-stable
  :config
  (global-set-key (kbd "<S-s-up>")     'buf-move-up)
  (global-set-key (kbd "<S-s-down>")   'buf-move-down)
  (global-set-key (kbd "<S-s-left>")   'buf-move-left)
  (global-set-key (kbd "<S-s-right>")  'buf-move-right)
  )

(use-package rainbow-delimiters
  :ensure t
  :pin melpa-stable
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
  )

(use-package htmlize
  :ensure f
  :pin manual
)

(use-package iedit
  :pin melpa-stable
  :ensure t)

(use-package olivetti
  :ensure t
  :pin melpa-stable
  :config (setq olivetti-body-width 90))

(use-package markdown-mode
  :ensure t
  :custom
  (markdown-asymmetric-header t)
  (markdown-header-scaling t)
  (markdown-header-scaling-values '(1.1 1.05 1.02 1.01 1.0 1.0))
  (markdown-unordered-list-item-prefix "  - ")
  :init (setq auto-mode-alist
	      (cons '("\\.mdml$" . markdown-mode) auto-mode-alist)))

(use-package markdown-toc
  :ensure t)

(use-package markdown-preview-mode
  :ensure t
  :init (setq markdown-preview-stylesheets ())
  )

(use-package pager
  :ensure t
  :bind (("M-<up>" . pager-row-up)
	 ("M-<down>" . pager-row-down)))

(use-package uptimes
  :ensure t
  :pin melpa-stable)

(use-package dired-quick-sort
  :ensure t
  :pin melpa-stable
  :init
  (dired-quick-sort-setup))

(use-package dired-narrow
  :ensure t)

(use-package dictcc
  :ensure t
  :pin melpa-stable
)

(use-package counsel-tramp
  :ensure t
  :pin melpa-stable
)

(use-package dictionary
  :ensure t
  :pin melpa-stable
  :config
  (setq dictionary-server "dict.org"))

(use-package diminish
  :ensure t
  :pin manual
  )

(use-package ibuffer-tramp
  :ensure t)

(use-package request
  :ensure t
  :pin nongnu-elpa)

(use-package valign
  :ensure t
  :pin gnu
  )

(use-package zenburn-theme
  :ensure t
  :pin nongnu-elpa)

(use-package smart-mode-line-powerline-theme
  :ensure t
  :pin melpa-stable
  )

(use-package smart-mode-line
  :ensure t
  :pin melpa-stable
  :init
  (setq sml/theme 'powerline)
  (sml/setup)
  )

(use-package beacon
  :ensure t
  :pin manual
  :config
  (beacon-mode 1)
  (setq beacon-dont-blink-commands '(pager-row-down pager-row-up)) 
  (setq beacon-lighter '"Λ")  
  (add-to-list 'beacon-dont-blink-major-modes 'Man-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'woman-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'shell-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'inferior-python-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'xkcd-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'pdf-view-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'term-mode)
  :diminish beacon-mode
  )

(use-package all-the-icons
  :ensure t
  :pin melpa-stable)

(use-package all-the-icons-dired
  :ensure t
  :pin melpa-stable
  :init
  (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
  :diminish all-the-icons-dired-mode)

(use-package xkcd
  :ensure t
  :pin melpa-stable)

(use-package fireplace
  :ensure t
  :pin melpa-stable)

;; (use-package post
;;   :ensure f
;;   :config
;;   (setq post-signature-pattern "\\(--\\|\\)")
;;   )

;; (use-package simple-wiki)

(use-package printing
  :config
  (pr-update-menus t))


(autoload 'muttrc-mode "muttrc-mode.el"
  "Major mode to edit muttrc files" t)

;; (setq auto-mode-alist
;;       (append '(("muttrc\\'" . muttrc-mode) )))

(add-to-list 'auto-mode-alist '("muttrc\\'" . muttrc-mode) t)

(require 'uniquify)

;; (setq auto-mode-alist
;;       (cons '("\\.org$" . org-mode) auto-mode-alist))

(add-to-list 'auto-mode-alist '("\\.org$" . org-mode) t)

(setq org-log-done t)

(eval-after-load 'org '(setf org-highlight-latex-and-related
'(latex)))

(add-hook 'org-mode-hook
'(lambda ()
       (setq org-file-apps
	     (append '(
		       ("\\.png\\'" . default)
		       ) org-file-apps ))))

(setq org-fontify-quote-and-verse-blocks t)

(when window-system
  (tooltip-mode -1)
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1))

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
;; (global-set-key "\C-cb" 'org-iswitchb)
(global-set-key (kbd "\C-c c") 'org-capture)
(global-unset-key "\C-z")

(global-set-key (kbd "<C-S-up>")     'windmove-up)
(global-set-key (kbd "<C-S-down>")   'windmove-down)
(global-set-key (kbd "<C-S-left>")   'windmove-left)
(global-set-key (kbd "<C-S-right>")  'windmove-right)

(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "--simple-prompt -i")

(setq ispell-dictionary "british")

(setq dired-listing-switches "-algo")

(add-hook 'dired-after-readin-hook 'hl-line-mode)

(global-set-key (kbd "<menu>") 'nil)

(blink-cursor-mode 0)

(setq shift-select-mode nil)

(setq mouse-yank-at-point t)

(setq transient-mark-mode t)

(show-paren-mode t)

(recentf-mode 1)

(global-set-key "\M- " 'hippie-expand)

(setq truncate-lines t)
(add-hook 'minibuffer-setup-hook
      (lambda () (setq truncate-lines nil)))

(setq kill-emacs-query-functions
      (cons (lambda () (yes-or-no-p "Really Quit Emacs? "))
	    kill-emacs-query-functions))

(put 'upcase-region 'disabled nil)

(desktop-save-mode 1)
(setq desktop-restore-eager 10)
(setq desktop-save t) ;; save without asking

(defalias 'list-buffers 'ibuffer)

(setq ibuffer-formats
      '((mark modified read-only " "
              (name 30 30 :left :elide) " "
              (size 9 -1 :right) " "
              (mode 16 16 :left :elide) " " filename-and-process)
        (mark " " (name 16 -1) " " filename)))

(define-ibuffer-sorter pathname
  "Sort by pathname"
  (:description "path")
  (cl-flet ((get-pathname
	     (data)
	     (with-current-buffer (car data)
	       (or buffer-file-name
		   (if (eq major-mode 'dired-mode)
		       (expand-file-name dired-directory))
		   ;; so that all non pathnames are at the end
		   ""))))
    (string< (get-pathname a) (get-pathname b))))

(define-key ibuffer-mode-map
  (kbd "s p") 'ibuffer-do-sort-by-pathname)

(defun get-all-buffer-directories ()
  "Return a list of all directories that have at least one
     file being visited."
  (interactive)
  (let (l)
    (dolist (e (sort (mapcar 'file-name-directory
			     (remove-if-not 'identity
					    (mapcar 'buffer-file-name
						    (buffer-list))))
		     'string<))
      (unless (string= (car l) e)
	(setq l (cons e l))))
    l))

(define-ibuffer-filter dirname
    "Toggle current view to buffers with in a directory DIRNAME."
  (:description "directory name"
		:reader
		(intern
		 (completing-read "Filter by directory: "
				  (get-all-buffer-directories)
				  'identity
				  t nil nil nil nil)))
  (string= qualifier
	   (and (buffer-file-name buf)
		(file-name-directory (buffer-file-name buf)))))

(defun ibuffer-set-filter-groups-by-directory ()
  "Set the current filter groups to filter by directory."
  (interactive)
  (setq ibuffer-filter-groups
	(mapcar (lambda (dir)
		  (cons (format "%s" dir) `((dirname . ,dir))))
		(get-all-buffer-directories)))
  (ibuffer-update nil t))

(define-key ibuffer-mode-map
  (kbd "/ D") 'ibuffer-set-filter-groups-by-directory)
(define-key ibuffer-mode-map
  (kbd "/ d") 'ibuffer-filter-by-dirname)

;; (electric-pair-mode 1)
;; (defvar markdown-electric-pairs '((?* . ?*)) "Electric pairs for markdown-mode.")
;; (defun markdown-add-electric-pairs ()
;;   (setq-local electric-pair-pairs (append electric-pair-pairs markdown-electric-pairs))
;;   (setq-local electric-pair-text-pairs electric-pair-pairs))
;; (add-hook 'markdown-mode-hook 'markdown-add-electric-pairs)

(advice-add 'linum-mode :override 'display-line-numbers-mode)

(setq org-capture-templates
      '(
	("b" "Bookmark" entry (file+headline "~/science_works/Org/bookmarks.org" "Bookmarks")
	 "* %(org-cliplink-capture) %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n" :empty-lines 1)
	("a" "arXiV" entry (file+headline "~/science_works/Org/arXiv.org" "arXiv preprints")
	 "* %(org-cliplink-capture) %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n" :empty-lines 1)
	("r" "Reading List" entry (file+headline "~/science_works/Org/arXiv.org" "Reading List")
	 "* %(org-cliplink-capture) %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n" :empty-lines 1)
	)
      )

(setq org-image-actual-width nil)

(eval-after-load 'all-the-icons
'(setq org-agenda-category-icon-alist 
      `(
	("talk"
	 ,(list
	   (all-the-icons-faicon "graduation-cap"  :face 'all-the-icons-red))
	 nil nil :ascent center)
	("meeting"
	 ,(list
	   (all-the-icons-material "person_add"  :face 'all-the-icons-yellow))
	 nil nil :ascent center)
	("flight"
	 ,(list
	   (all-the-icons-faicon "plane"  :face 'all-the-icons-blue))
	 nil nil :ascent center)
	("train"
	 ,(list
	   (all-the-icons-faicon "train"  :face 'all-the-icons-blue))
	 nil nil :ascent center)
	("mail"
	 ,(list
	   (all-the-icons-octicon "mail"  :face 'all-the-icons-yellow))
	 nil nil :ascent center)
	("observe"
	 ,(list
	   (all-the-icons-octicon "telescope"  :face 'all-the-icons-yellow))
	 nil nil :ascent center)
	("medical"
	 ,(list
	   (all-the-icons-octicon "pulse"  :face 'all-the-icons-red))
	 nil nil :ascent center)
	("paper"
	 ,(list
	   (all-the-icons-faicon "newspaper-o"  :face 'all-the-icons-red))
	 nil nil :ascent center)
       )
      )
)

(setq use-file-dialog 'nil)

(defun timestamp ()
  (interactive)
  (insert (format-time-string "%d.%m.%Y, %H:%M")))

(defun my-count-words-region (posBegin posEnd)
  "Print number of words and chars in region."
  (interactive "r")
  (message "Counting …")
  (save-excursion
    (let (wordCount charCount)
      (setq wordCount 0)
      (setq charCount (- posEnd posBegin))
      (goto-char posBegin)
      (while (and (< (point) posEnd)
                  (re-search-forward "\\w+\\W*" posEnd t))
        (setq wordCount (1+ wordCount)))

      (message "Words: %d. Chars: %d." wordCount charCount)
      )))

(defun unfill-paragraph ()
  "Replace newline chars in current paragraph by single spaces.
This command does the inverse of `fill-paragraph'."
  (interactive)
  (let ((fill-column 90002000)) ; 90002000 is just random. you can use `most-positive-fixnum'
    (fill-paragraph nil)))
(defun unfill-region (start end)
  "Replace newline chars in region by single spaces.
This command does the inverse of `fill-region'."
  (interactive "r")
  (let ((fill-column 90002000))
    (fill-region start end)))

;; remember regions
(global-set-key (kbd "<f6>") 'set-markers-for-region)
(defun set-markers-for-region ()
  (interactive)
  (make-local-variable 'm1)
  (make-local-variable 'm2)
  (setq m1 (copy-marker (mark)))
  (setq m2 (copy-marker (point))))

(global-set-key (kbd "<C-f6>") 'set-region-from-markers)
(defun set-region-from-markers ()
  (interactive)
  (set-mark m1)
  (goto-char m2))

(global-set-key (kbd "<C-S-f6>") 'unset-region-markers)
(defun unset-region-markers ()
  (interactive)
  (set-marker m1 nil)
  (set-marker m2 nil))

(add-to-list 'auto-mode-alist '("/tmp/neomutt-*" . mail-mode) t)
(add-hook 'mail-mode-hook 'turn-on-auto-fill)

